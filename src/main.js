import './scss/index.scss';
import 'fullpage.js/dist/fullpage.css';

import 'babel-polyfill';
import Vue from 'vue';
import App from './App.vue';
import SplitWords from './mixins/SplitWords';
import Animating from './mixins/Animating';

import 'fullpage.js/vendors/scrolloverflow';
import VueFullPage from 'vue-fullpage.js/dist/vue-fullpage.min';
import BootstrapVue from 'bootstrap-vue';
import VueRouter from 'vue-router'

Vue.use(VueRouter)
Vue.use(BootstrapVue);
Vue.use(VueFullPage);

Vue.config.productionTip = false;

Vue.mixin(SplitWords);
Vue.mixin(Animating);

const router = new VueRouter({
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
