export default {
    methods: {
        splitWords: function (selector) {
            let words = document.querySelectorAll(selector);

            Array.prototype.forEach.call(words, function (quote) {
                quote.innerText.replace(/(<([^>]+)>)/ig, "");
                let quotewords = quote.innerText.split(" ");
                let wordCount = quotewords.length;
                quote.innerHTML = "";
                for (let i = 0; i < wordCount; i++) {
                    quote.innerHTML += "<span><span>" + quotewords[i] +
                        "</span></span>";
                    if (i < quotewords.length - 1) {
                        quote.innerHTML += " ";
                    }
                }
            })


        },
        getRandom: function (min, max) {
            return Math.random() * (max - min) + min;
        }
    }
};
