import $ from 'jquery';
import 'jquery.transit'

export default {
    data: function () {
        return {
            isAnimating: false
        };
    },
    methods: {
        animateDiv: function (value) {
            this.isAnimating = value
        },
        loadWords: function (words) {
            Array.prototype.forEach.call(words, function (word) {
                $(word).find('span').transition({ y: '0' }, 1000);
            })
        },
        hideWords: function (words) {
            Array.prototype.forEach.call(words, function (word) {
                $(word).find('span').transition({ y: '100%' }, 1);
            })
        },
    },
    watch: {
        isAnimating: function (val) {
            const words = document.querySelectorAll(this.sectionSelector + " .content span");
            if (val) {
                this.loadWords(words);
            } else {
                this.hideWords(words);
            }
        }
    }
};
